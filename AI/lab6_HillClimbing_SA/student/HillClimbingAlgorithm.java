package student;

public class HillClimbingAlgorithm {

	public static Node excute(Node start) {
		Node current = new Node(start);
		while (true) {
			Node next = current.getBest();
			if (current.getH() > next.getH())
				current = next;
			else
				return current;

		}

	}

	public static Node excuteHillClimbingAlgorithm(Node start) {
		Node re = excute(start);
		while (re.getH() != 0) {
			re = new Node();
			re.generateBoard();
			re = excute(start);
		}
		return re;

	}

	public static void main(String[] args) {
		Node n = new Node();
		n.generateBoard();
		n.displayBoard();
		Node node = excute(n);
		System.out.println(n.getH());

		Node node2 = excuteHillClimbingAlgorithm(n);
//		System.out.println(n.getH());

		n.selectNextRandomCandidate().displayBoard();

	}

}
