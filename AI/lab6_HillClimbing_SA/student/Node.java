package student;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Node {
	public static final int N = 8;
	private Queen[] state;

	public Node() {
		// generateBoard();
		state = new Queen[N];
	}

	public Node(Queen[] state) {
		this.state = new Queen[N];
		for (int i = 0; i < state.length; i++) {
			this.state[i] = new Queen(state[i].getRow(), state[i].getColumn());
		}
	}

	public Node(Node n) {
		this.state = new Queen[N];
		for (int i = 0; i < N; i++) {
			Queen qi = n.state[i];
			this.state[i] = new Queen(qi.getRow(), qi.getColumn());
		}
	}

	public void generateBoard() {
		Random random = new Random();
		for (int i = 0; i < N; i++) {
			state[i] = new Queen(random.nextInt(N), i);
		}
	}

	public int getH() {
		int heuristic = 0;
		for (int i = 0; i < state.length; i++) {
			for (int j = i + 1; j < state.length; j++) {
				if (state[i].isConflict(state[j]))
					heuristic++;
			}

		}
		return heuristic;
	}

	public List<Node> generateAllCandidates() {
		List<Node> result = new ArrayList<Node>();
		for (int i = 0; i < state.length; i++) {
			Node tmp = new Node(state);
			tmp.state[i].move();
			result.add(tmp);
		}

		return result;
	}

	public Node selectNextRandomCandidate() {
		Node result = new Node(state);
		Random r = new Random();
		int random = r.nextInt(N);
		result.state[random].setRow(r.nextInt(N));
		return result;
	}

	public void displayBoard() {
		int[][] board = new int[N][N];
		// set queen position on the board
		for (int i = 0; i < N; i++) {
			board[state[i].getRow()][state[i].getColumn()] = 1;
		}
		// print board
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (board[i][j] == 1) {
					System.out.print("Q" + " ");
				} else {
					System.out.print("-" + " ");
				}
			}
			System.out.println();
		}
	}

	public Node getBest() {
		List<Node> node = generateAllCandidates();
		Node min = node.get(0);
		for (Node n : node) {
			if (n.getH() < min.getH())
				min = n;
		}
		return min;
	}

	public static void main(String[] args) {
		Node n = new Node();
		n.generateBoard();
		n.displayBoard();
		System.out.println(n.getH());

		n.selectNextRandomCandidate().displayBoard();

	}

}
