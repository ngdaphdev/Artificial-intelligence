package lab2_3_BFS_DFS_UCS;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BreadthFirstSearchAlgo implements ISearchAlgo {
	public Node execute(Node root, String goal) {
		Queue<Node> frontier = new LinkedList<>();
		frontier.add(root);
		List<Node> exploerd = new ArrayList<>();
		while (!frontier.isEmpty()) {
			Node current = frontier.peek();
			exploerd.add(current);
			frontier.poll();
			
			if (current.getLabel().equals(goal)) return current;
			System.out.println(current.getLabel());
			List<Node> children = current.getChildrenNodes();
			for (Node child : children) {
				if (!(frontier.contains(child) && !exploerd.contains(child))) {
					frontier.add(child);
					child.setParent(current);
				}
			}
		}
		return null;
	}
	@Override
	public Node execute(Node root, String start, String goal) {
	
		Queue<Node> frontier = new LinkedList<>();
		frontier.add(root);
		boolean checkstart =false;
		List<Node> exploerd = new ArrayList<>();
		while (!frontier.isEmpty()) {
			Node current = frontier.peek();
			exploerd.add(current);
			frontier.poll();
			
			if (current.getLabel().equals(goal)&& checkstart) return current;
			System.out.println(current.getLabel());
			List<Node> children = current.getChildrenNodes();
			for (Node child : children) {
				if (!(frontier.contains(child) && !exploerd.contains(child))) {
					frontier.add(child);
					child.setParent(current);
				}
			}
		}
		return null;
	}
	@Override
	public Node execute(Node root, String goal, int limitedDepth) {
		// TODO Auto-generated method stub
		return null;
	}
}