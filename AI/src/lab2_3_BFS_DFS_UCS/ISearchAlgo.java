package lab2_3_BFS_DFS_UCS;

public interface ISearchAlgo {
	 Node execute(Node root, String goal);// find the path from root node to the goal node

	 Node execute(Node root, String start, String goal); // find the path from start node to the goal node
	
	Node execute(Node root, String goal, int limitedDepth);
		
	}

