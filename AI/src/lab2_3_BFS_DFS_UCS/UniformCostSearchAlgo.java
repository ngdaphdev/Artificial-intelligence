package lab2_3_BFS_DFS_UCS;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class UniformCostSearchAlgo implements ISearchAlgo {

	@Override
	public Node execute(Node root, String goal) {
		PriorityQueue<Node> frontier = new PriorityQueue<Node>(new NodeComparator());
		frontier.add(root);
		List<Node> exploerd = new ArrayList<Node>();

		while (!frontier.isEmpty()) {
			Node current = frontier.peek();
			exploerd.add(current);
			frontier.poll();
			if (current.getLabel().equals(goal))
				return current;
			else {
				List<Edge> chilrend = current.getChildren();
				for (Edge child : chilrend) {
					Node end = child.getEnd();
					double pathCost = current.getPathCost() + child.getWeight();
					if (!frontier.contains(end) && !exploerd.contains(end)) {
						frontier.add(end);
						end.setPathCost(pathCost);
						end.setParent(current);
					} else if (end.getPathCost() >= (pathCost)) {
						end.setPathCost(pathCost);
						end.setParent(current);
					}

				}
			}
		}
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node execute(Node root, String start, String goal) {

		PriorityQueue<Node> frontier = new PriorityQueue<Node>(new NodeComparator());
		frontier.add(root);
		List<Node> exploerd = new ArrayList<Node>();
		boolean checkstart = false;

		while (!frontier.isEmpty()) {
			Node current = frontier.peek();
			exploerd.add(current);
			frontier.poll();
			if (current.getLabel().equals(start)) {
				checkstart = true;
				frontier.clear();
				exploerd.clear();
			}
			if (current.getLabel().equals(goal) && checkstart)
				return current;

			else {
				List<Node> childrend = current.getChildrenNodes();
				for (Node child : childrend) {

					if (!frontier.contains(child) && !exploerd.contains(child)) {
						frontier.add(child);
						if (checkstart)
							child.setParent(current);

					}

				}
			}
		}
		return null;
	}

	@Override
	public Node execute(Node root, String goal, int limitedDepth) {
		// TODO Auto-generated method stub
		return null;
	}

}
