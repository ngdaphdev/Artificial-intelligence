package lab4_InformedSearch;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;





public class GreedyBestFirstSearchAlgo implements IInformedSearchAlgo {
	// task1
	@Override
	public Node execute(Node root, String goal) {
		Queue<Node> frontier = new LinkedList<>();
		frontier.add(root);
		List<Node> exploerd = new ArrayList<>();
		while (!frontier.isEmpty()) {
			Node current = frontier.peek();
			exploerd.add(current);
			frontier.poll();

			if (current.getLabel().equals(goal))
				return current;

			List<Edge> children = current.getChildren();
			for (Edge child : children) {
				Node nodeEnd = child.getEnd();
				if (!(frontier.contains(nodeEnd) && !exploerd.contains(nodeEnd))) {
					nodeEnd.setG(current.getG() + child.getWeight());
					nodeEnd.setParent(current);
					frontier.add(nodeEnd);

				}
			}
		}
		return null;
	}

	@Override
	public Node execute(Node root, String start, String goal) {
		Queue<Node> frontier = new LinkedList<>();
		frontier.add(root);
		boolean checkstart =false;
		List<Node> exploerd = new ArrayList<>();
		while (!frontier.isEmpty()) {
			Node current = frontier.peek();
			exploerd.add(current);
			frontier.poll();
			
			if (current.getLabel().equals(goal)&& checkstart) return current;
			System.out.println(current.getLabel());
			List<Edge> children = current.getChildren();
			for (Edge child : children) {
				Node nodeEnd = child.getEnd();
				if (!(frontier.contains(nodeEnd) && !exploerd.contains(nodeEnd))) {
					nodeEnd.setG(current.getG() + child.getWeight());
					nodeEnd.setParent(current);
					frontier.add(nodeEnd);
				}
			}
		}
		return null;
	}

}
