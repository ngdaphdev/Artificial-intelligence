package lab5_InformedSearch_8_Puzzle;

public interface IPuzzleAlgo {
	public Node execute(Puzzle model);
}
